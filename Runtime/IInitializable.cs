﻿namespace Skrimel.Gist.Backbone
{
    public interface IInitializable
    {
        void Initialize();
    }
}