﻿using System;
using Sirenix.OdinInspector;
using Skrimel.Gist.Backbone.Identifiers;
using UnityEngine;
using UnityEngine.Serialization;

namespace Skrimel.Gist.Backbone
{
    [Serializable]
    public class IdentifiedObject : IIdentified
    {
        [FormerlySerializedAs("_identifier")] [SerializeField, ReadOnly] private Id _id = Id.Regular;
        public Id Id => _id;

        protected void SetIdentifier(Id id) =>
            _id = id;
    }
}