﻿using Skrimel.Gist.BackBone.ContentBase;

namespace Skrimel.Gist.Backbone.User
{
    public class UserCreator : Creator
    {
        protected override IContent CreateConcreteInstance() =>
            new User();
    }
}