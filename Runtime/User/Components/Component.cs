﻿using Skrimel.Gist.BackBone.ContentBase;

namespace Skrimel.Gist.Backbone.User.Components
{
    public class Component<TCreator> : Content<TCreator>
        where TCreator : ComponentModel
    {
        
    }
}