﻿using System.Collections.Generic;

namespace Skrimel.Gist.Backbone.User.Components
{
    public interface IComponentModel
    {
        IEnumerable<IComponent> RequiredComponents { get; }
    }
}