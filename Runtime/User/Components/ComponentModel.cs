﻿using System.Collections.Generic;
using System.Linq;
using Skrimel.Gist.BackBone.ContentBase;
using UnityEngine;

namespace Skrimel.Gist.Backbone.User.Components
{
    public abstract class ComponentModel : Creator, IComponent
    {
        [SerializeField] private List<ComponentModel> _requiredComponents = default;
        public IEnumerable<IComponent> RequiredComponents => _requiredComponents;

        public bool IsRequirementsFulfilled(IEnumerable<IComponent> loadedComponents) =>
            !_requiredComponents.Except(loadedComponents).Any();
    }
}