using System;

namespace Skrimel.Gist.Backbone.RX.Reactive
{
    public interface IReadonlyReactive<TData>
    {
        event Action<TData> DataChanged; 
        event Action<TData> DataSet;
        TData Data { get; }
    }
}
