using System;
using UnityEngine;

namespace Skrimel.Gist.Backbone.RX.Reactive
{
    [Serializable]
    public class Reactive<TDataType> : IReadonlyReactive<TDataType>
    {
        [SerializeField] private TDataType _data = default;
        [field: NonSerialized] public event Action<TDataType> DataChanged;
        [field: NonSerialized] public event Action<TDataType> DataSet;

        public TDataType Data
        {
            get => _data;
            set
            {
                var oldData = _data;
                _data = value;

                DataSetHandler();

                if (oldData != null && _data != null)
                    if (!_data.Equals(oldData))
                        DataChangedHandler();
            }
        }

        public Reactive(TDataType data)
        {
            _data = data;
        }

        public Reactive() : this(default) {}

        public static implicit operator TDataType(Reactive<TDataType> container) => container.Data;

        private void DataSetHandler()
        {
            try
            {
                DataSet?.Invoke(_data);
            }
            catch (Exception e)
            {
                Debug.LogError(e.ToString());
            }
        }

        private void DataChangedHandler()
        {
            try
            {
                DataChanged?.Invoke(_data);
            }
            catch (Exception e)
            {
                Debug.LogError(e.ToString());
            }
        }
    }
}
