﻿using System;
using System.Collections.Generic;

namespace Skrimel.Gist.Backbone
{
    public interface IScriptableTable
    {
        Type ContentType { get; }
        
        TItem FindItem<TItem>() where TItem : class;
        TItem FindItem<TItem>(Predicate<TItem> filter) where TItem : class;
        
        IEnumerable<TItem> GetItems<TItem>() where TItem : class;
        IEnumerable<TItem> GetItems<TItem>(Predicate<TItem> filter) where TItem : class;
        
#if UNITY_EDITOR
        void SetContent<TItem>(IEnumerable<TItem> items)
            where TItem : UnityEngine.Object;
#endif
    }
}