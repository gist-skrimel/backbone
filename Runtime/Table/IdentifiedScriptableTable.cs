﻿using Skrimel.Gist.Backbone.Identifiers;

namespace Skrimel.Gist.Backbone
{
    public partial class IdentifiedScriptableTable<TItem> : ScriptableTable<TItem>, IDependencyProvider<TItem>
        where TItem : IdentifiedScriptableObject
    {
        public TItem FindItem(Id id) =>
            base.FindItem(item => item.Id == id);
        
        public TType FindItem<TType>(Id id)
            where TType : TItem =>
            FindItem<TType>(item => item.Id == id);
        
        public TItem this[string guid] => FindItem(new Id(guid));
        public TItem this[Id id] => FindItem(id);
        
        public bool Contains(Id id)
        {
            foreach (var item in _items)
                if (item.Id == id)
                    return true;
            
            return false;
        }
    }
}