using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.Utilities;
using UnityEngine;

namespace Skrimel.Gist.Backbone
{
    [Serializable]
    public class ScriptableTable<TItem> : IdentifiedScriptableObject, IEnumerable<TItem>, IScriptableTable
        where TItem : UnityEngine.Object
    {
        [SerializeField] protected List<TItem> _items = new List<TItem>();
        public IReadOnlyList<TItem> Items => _items;

        public Type ContentType => typeof(TItem);

#if UNITY_EDITOR
        protected virtual bool CheckItemSuitability(TItem item) => true;
        
        public void SetContent<TInputItem>(IEnumerable<TInputItem> items)
            where TInputItem : UnityEngine.Object
        {
            var castedItems = new List<TItem>();
            
            foreach (var item in items)
            {
                if (item is TItem castedItem)
                {
                    if (CheckItemSuitability(castedItem))
                        castedItems.Add(castedItem);
                }
                else
                {
                    Debug.Log($"Unable to cast item type {item.GetType()} to required {typeof(TItem)}.",
                        item);
                    throw new InvalidCastException("Unable to perform cast!");
                }
            }
            
            _items.Clear();
            _items.AddRange(castedItems);
        }
#endif

        public IEnumerator GetEnumerator() =>
            Items.GetEnumerator();
        
        IEnumerator<TItem> IEnumerable<TItem>.GetEnumerator() =>
            Items.GetEnumerator();
        
        public IEnumerable<TItem> GetItems(Predicate<TItem> filter) =>
            _items.FindAll(filter);

        public IEnumerable<TType> GetItems<TType>(Predicate<TType> filter)
            where TType : class
        {
            return GetItems(item => item is TType && filter(item as TType))
                .Convert(item => item as TType);
        }
        
        public IEnumerable<TType> GetItems<TType>() where TType : class =>
            GetItems<TType>(item => true);
        
        public TItem FindItem(Predicate<TItem> comparer)
        {
            var result = _items.Find(comparer);
            
            if (result == default)
                throw new NullReferenceException("Element not found");
            
            return result;
        }
        
        public TType FindItem<TType>(Predicate<TType> comparer)
            where TType : class
        {
            var result = GetItems(comparer);
            
            if (!result.Any())
                throw new NullReferenceException("Element not found");
            
            return result.First();
        }
        
        public TType FindItem<TType>()
            where TType : class
        {
            return FindItem<TType>(item => true);
        }
        
        public bool TryAdd(TItem element)
        {
            if (_items.Contains(element))
                return false;
            
            _items.Add(element); 
            return true;
        }
        
        public int IndexOf(TItem element) => 
            _items.IndexOf(element);
        
        public bool Contains(TItem targetItem)
        {
            foreach (var item in _items)
                if (item == targetItem)
                    return true;
            
            return false;
        }
    }
}