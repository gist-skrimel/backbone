namespace Skrimel.Gist.Backbone
{
    public interface IState<TItem, TCaretaker, TDependencyBase>
        where TCaretaker : IDependencyProvider<TDependencyBase>
    {
        void CopyState(TItem item);
        TItem PasteState(TItem item, TCaretaker caretaker);
    }
}
