﻿namespace Skrimel.Gist.Backbone
{
    public class SaveableScriptableObject<TElement, TState, TDependencyProvider, TDependencyBase>
        : IdentifiedScriptableObject, IMemento<TElement, TState, TDependencyProvider, TDependencyBase>
        where TElement : SaveableScriptableObject<TElement, TState, TDependencyProvider, TDependencyBase>
        where TState : IState<TElement, TDependencyProvider, TDependencyBase>, new()
        where TDependencyProvider : IDependencyProvider<TDependencyBase>
    {
        public TState GetState()
        {
            var result = new TState();
            result.CopyState((TElement)this);
            return result;
        }
        public void SetState(TState stateSave, TDependencyProvider dependencyProvider) =>
            stateSave.PasteState((TElement)this, dependencyProvider);
    }
}