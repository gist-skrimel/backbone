namespace Skrimel.Gist.Backbone
{
    public class SaveableObject<TElement, TState, TCaretaker, TDependencyBase>
        : IMemento<TElement, TState, TCaretaker, TDependencyBase>
        where TElement : SaveableObject<TElement, TState, TCaretaker, TDependencyBase>
        where TState : IState<TElement, TCaretaker, TDependencyBase>, new()
        where TCaretaker : IDependencyProvider<TDependencyBase>
    {
        public TState GetState()
        {
            var result = new TState();
            result.CopyState((TElement)this);
            return result;
        }
        public void SetState(TState stateSave, TCaretaker dependencyProvider) =>
            stateSave.PasteState((TElement)this, dependencyProvider);
    }
}
