﻿using Skrimel.Gist.Backbone.Identifiers;

namespace Skrimel.Gist.Backbone
{
    public interface IDependencyProvider<TItemBase>
    {
        TItem FindItem<TItem>(Id id) where TItem : TItemBase;
    }
}