namespace Skrimel.Gist.Backbone
{
    public interface IMemento<TItem, TState, TDependencyProvider, TDependencyBase>
        where TState : IState<TItem, TDependencyProvider, TDependencyBase>
        where TDependencyProvider : IDependencyProvider<TDependencyBase>
    {
        TState GetState();
        void SetState(TState stateSave, TDependencyProvider dependencyProvider);
    }
}
