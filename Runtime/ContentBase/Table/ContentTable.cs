﻿using System;
using Skrimel.Gist.Backbone;
using Skrimel.Gist.Backbone.Identifiers;
using Skrimel.Gist.Economics;
using UnityEngine;

namespace Skrimel.Gist.BackBone.ContentBase
{
    [CreateAssetMenu(fileName = "Table", menuName = "Content/Content Table")]
    [Serializable]
    public class ContentTable : IdentifiedScriptableTable<Creator>, IDependencyProvider<IContent>
    {
        [SerializeField] private CurrenciesCollection _currenciesCollection = default;
        public CurrenciesCollection CurrenciesCollection => _currenciesCollection;

        TContentItem IDependencyProvider<IContent>.FindItem<TContentItem>(Id id)
        {
            var model = base.FindItem<Creator>(id);
            return model.Create<TContentItem>();
        }
    }
}
