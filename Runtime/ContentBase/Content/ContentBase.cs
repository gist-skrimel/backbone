﻿using System;
using Skrimel.Gist.Backbone;
using Skrimel.Gist.Backbone.Identifiers;
using UnityEditor;

namespace Skrimel.Gist.BackBone.ContentBase
{
    public abstract class ContentBase : SaveableObject<ContentBase, ContentBase.State, ContentTable, IContent>
    {
        public abstract Id Id { get; }
        
        public virtual string GetPayload() =>
            "";
        public virtual void SetPayload(string payload, ContentTable caretaker) {}
        
        [Serializable]
        public struct State : IState<ContentBase, ContentTable, IContent>
        {
            public Id id;
            public string payload;

            public void CopyState(ContentBase item)
            {
                id = item.Id;
                payload = item.GetPayload();
            }
            
            public ContentBase PasteState(ContentBase item, ContentTable caretaker)
            {
                if (item.Id != id)
                    throw new OperationCanceledException();
                
                item.SetPayload(payload, caretaker);
                return item;
            }
        }
    }
}