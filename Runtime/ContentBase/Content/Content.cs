﻿using System;
using Skrimel.Gist.Backbone;
using Skrimel.Gist.Backbone.Identifiers;

namespace Skrimel.Gist.BackBone.ContentBase
{
    public abstract class Content<TModel> : ContentBase, IContent
        where TModel : class, ICreator
    {
        protected TModel _model = default;
        public ICreator Model => _model;
        public TModel SpecificModel => _model;
        public override Id Id => SpecificModel.Id;

        public event Action ModelSet; 
        
        public void SetModel(ICreator model)
        {
            if (_model != default)
                throw new OperationCanceledException("Model is already set!");

            if (model == default)
                throw new NullReferenceException("Null model can not be set");
            
            if (!(model is TModel))
                throw new OperationCanceledException("Not correct model type catched");
            
            _model = (model as TModel);
            ModelSet?.Invoke();
        }
    }
}