﻿using Skrimel.Gist.Backbone;
using Skrimel.Gist.Backbone.Identifiers;

namespace Skrimel.Gist.BackBone.ContentBase
{
    public interface IContentBase : IIdentified, IMemento<ContentBase, ContentBase.State, ContentTable, IContent>
    {
        void SetPayload(string payload, ContentTable caretaker);
        string GetPayload();
        
        void SetModel(ICreator model);
    }
}