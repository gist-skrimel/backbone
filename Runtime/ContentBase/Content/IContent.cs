﻿using System;

namespace Skrimel.Gist.BackBone.ContentBase
{
    public interface IContent : IContentBase
    {
        event Action ModelSet;
        ICreator Model { get; }
    }
}