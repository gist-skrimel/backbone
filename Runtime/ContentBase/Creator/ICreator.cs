﻿using Skrimel.Gist.Backbone.Identifiers;

namespace Skrimel.Gist.BackBone.ContentBase
{
    public interface ICreator : IIdentified
    {
        IContent Create();
        TCastedOutput Create<TCastedOutput>() where TCastedOutput : IContent;
    }
}