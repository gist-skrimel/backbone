﻿using System;
using Skrimel.Gist.Backbone;

namespace Skrimel.Gist.BackBone.ContentBase
{
    public abstract class Creator : IdentifiedScriptableObject, ICreator
    {
        public IContent Create()
        {
            var result = CreateConcreteInstance();
            result.SetModel(this);
            return result;
        }
        
        public TCastedOutput Create<TCastedOutput>()
            where TCastedOutput : IContent
        {
            var instance = Create();
            
            if (!(instance is TCastedOutput output))
                throw new InvalidCastException("Unable to cast");
            
            return output;
        }
        
        protected abstract IContent CreateConcreteInstance();
    }
}
