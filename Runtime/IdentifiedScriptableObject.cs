﻿using Sirenix.OdinInspector;
using Skrimel.Gist.Backbone.Identifiers;
using UnityEngine;
using UnityEngine.Serialization;

namespace Skrimel.Gist.Backbone
{
    public abstract class IdentifiedScriptableObject : ScriptableObject, IIdentified
    {
        [FormerlySerializedAs("_identifier")] [SerializeField, ReadOnly] private Id _id = Id.Regular;
        public Id Id => _id;
    }
}
