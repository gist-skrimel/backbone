namespace Skrimel.Gist.Backbone.Identifiers
{
    public interface IIdentified
    {
        Id Id { get; }
    }
}