﻿using Skrimel.Gist.Backbone;
using UnityEngine;

namespace Skrimel.Gist.Economics
{
    [CreateAssetMenu(fileName = "Currencies Collection", menuName = "Hood/Economic/Currencies collection", order = 0)]
    public class CurrenciesCollection : IdentifiedScriptableTable<Currency> {}
}