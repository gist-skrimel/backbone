﻿using Skrimel.Gist.Backbone;
using UnityEngine;

namespace Skrimel.Gist.Economics
{
    [CreateAssetMenu(fileName = "Currency", menuName = "Hood/Economic/Currency", order = 1)]
    public class Currency : IdentifiedScriptableObject
    {
        [SerializeField] private Sprite _icon = default;
        public Sprite Icon => _icon;
    }
}
