﻿using System;
using UnityEngine;

namespace Skrimel.Gist.Economics
{
    [Serializable]
    public struct CurrencyVolume
    {
        public Currency Currency;
        public uint Volume;

        public CurrencyVolume(Currency currency, uint volume)
        {
            Currency = currency;
            Volume = volume;
        }
    }
}
