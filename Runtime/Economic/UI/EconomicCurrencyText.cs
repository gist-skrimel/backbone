﻿using System;
using Skrimel.Gist.Economics;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

namespace Skrimel.Gist.Economic.UI
{
    public abstract class EconomicCurrencyText : MonoBehaviour
    {
        [SerializeField] private Text _text = default;
        [SerializeField] private Currency _currency = default;
        
        protected abstract EconomicAccount GetTarget();

        [NonSerialized] private EconomicAccount _target = default;
        
        private void UpdateText()
        {
            _text.text = _target[_currency].ToString();
        }
        
        private void Awake()
        {
            _target = GetTarget();
            UpdateText();
            
            _target.TransactionProcessed += UpdateText;
        }
        
        private void OnDestroy()
        {
            _target.TransactionProcessed -= UpdateText;
        }
    }
}