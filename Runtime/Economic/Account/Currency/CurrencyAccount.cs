﻿using System;
using Skrimel.Gist.Backbone;
using Skrimel.Gist.Backbone.Identifiers;
using UnityEngine;
using UnityEngine.Serialization;

namespace Skrimel.Gist.Economics
{
    [Serializable]
    public struct CurrencyAccount : IMemento<CurrencyAccount, CurrencyAccount.State, CurrenciesCollection, Currency>,
        ICurrencyAccount
    {
        [SerializeField] private Currency _currency;
        public Currency Currency => _currency;
        
        [SerializeField] private uint _amount;
        public uint Amount => _amount;
        
        public event Action AmountChanged; 
        
        public CurrencyAccount(Currency currency, uint amount)
        {
            _currency = currency;
            _amount = amount;
            
            AmountChanged = () => {};
        }
        
        public CurrencyAccount(CurrencyVolume volume) : this(volume.Currency, volume.Volume) {}
        
        public bool CanPay(CurrencyVolume volume) =>
            volume.Currency == Currency && volume.Volume <= Amount;
        
        public bool CanProcessSuccessful(Transaction transaction)
        {
            var result = true;
            
            result &= (transaction.IsWithdraw && CanPay(transaction.Content)) || transaction.IsDeposit;
            result &= transaction.IsPending;
            
            return result;
        }
        
        public void Process(Transaction transaction)
        {
            if (transaction.Content.Currency != Currency)
            {
                transaction.SetStatus(TransactionStatus.Failed);
                throw new OperationCanceledException("Transaction currency mismatch");
            }
            
            if (transaction.IsCompleted)
            {
                transaction.SetStatus(TransactionStatus.Failed);
                throw new OperationCanceledException("Transaction is already completed");
            }
            
            if (transaction.IsWithdraw && transaction.Content.Volume > Amount)
            {
                transaction.SetStatus(TransactionStatus.Failed);
                throw new OperationCanceledException("Insufficient funds");
            }
            
            if (transaction.IsDeposit)
                _amount += transaction.Content.Volume;
            else
                _amount -= transaction.Content.Volume;
            
            transaction.SetStatus(TransactionStatus.Succeed);
            AmountChanged?.Invoke();
        }
        
        public State GetState()
        {
            var result = new State();
            result.CopyState(this);
            return result;
        }
        
        public void SetState(State stateSave, CurrenciesCollection dependencyProvider) =>
            this = stateSave.PasteState(this, dependencyProvider);
        
        [Serializable]
        public struct State : IState<CurrencyAccount, CurrenciesCollection, Currency>
        {
            [FormerlySerializedAs("currencyIdentifier")] public Id _currencyId;
            
            public uint amount;
            
            public void CopyState(CurrencyAccount item)
            {
                _currencyId = item.Currency.Id;
                amount = item.Amount;
            }

            public CurrencyAccount PasteState(CurrencyAccount item, CurrenciesCollection caretaker)
            {
                if (item.Currency.Id != _currencyId)
                    throw new OperationCanceledException("Identifiers mismatch");
                
                item._amount = amount;
                return item;
            }
        }
    }
}
