﻿using System;

namespace Skrimel.Gist.Economics
{
    public interface ICurrencyAccount
    {
        uint Amount { get; }
        Currency Currency { get; }
        event Action AmountChanged;
        
        bool CanPay(CurrencyVolume volume);
        bool CanProcessSuccessful(Transaction transaction);
        void Process(Transaction transaction);
    }
}