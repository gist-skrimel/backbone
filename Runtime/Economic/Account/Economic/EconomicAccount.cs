﻿using System;
using System.Collections.Generic;
using System.Linq;
using Skrimel.Gist.Backbone;
using Skrimel.Gist.Backbone.Identifiers;
using UnityEngine;

namespace Skrimel.Gist.Economics
{
    [Serializable]
    public class EconomicAccount : IEconomicAccount,
        IMemento<EconomicAccount, EconomicAccount.State, CurrenciesCollection, Currency>
    {
        [SerializeField] private List<CurrencyAccount> _accounts = new List<CurrencyAccount>();
        [NonSerialized] private List<CurrencyAccount.State> _unusedStates = new List<CurrencyAccount.State>();

        private CurrencyAccount this[Id id]
        {
            get => _accounts.First(item => item.Currency.Id == id);
            set => _accounts[_accounts.IndexOf(this[id])] = value;
        }

        public uint this[Currency currency] =>
            this[currency.Id].Amount;
        
        public event Action TransactionProcessed = () => {};
        
        public void Process(Transaction transaction)
        {
            var account = this[transaction.Content.Currency.Id];
            
            if (account.CanProcessSuccessful(transaction))
            {
                _accounts.Remove(account);
                account.Process(transaction);
                _accounts.Add(account);
                
                TransactionProcessed?.Invoke();
            }
            else
                throw new OperationCanceledException("Transaction can not be proceed successful");
        }
        
        public bool Contains(Currency currency) =>
            Contains(currency.Id);
        
        public bool Contains(Id id) =>
            _accounts.Any(item => item.Currency.Id == id);
         
        public bool OverlapsVolume(CurrencyVolume volume) =>
            Contains(volume.Currency) && volume.Volume <= this[volume.Currency];
        
        public State GetState()
        {
            var result = new State();
            result.CopyState(this);
            return result;
        }
        
        public void SetState(State stateSave, CurrenciesCollection dependencyProvider) =>
            stateSave.PasteState(this, dependencyProvider);
        
        [Serializable]
        public struct State : IState<EconomicAccount, CurrenciesCollection, Currency>
        {
            public List<CurrencyAccount.State> accountStates;
            
            public void CopyState(EconomicAccount item)
            {
                accountStates = new List<CurrencyAccount.State>();

                foreach (var currencyAccount in item._accounts)
                    accountStates.Add(currencyAccount.GetState());
                
                accountStates.AddRange(item._unusedStates);
            }

            public EconomicAccount PasteState(EconomicAccount item, CurrenciesCollection caretaker)
            {
                var unusedStates = new List<CurrencyAccount.State>();
                
                foreach (var state in accountStates)
                {
                    if (item.Contains(state._currencyId))
                    {
                        var account = item[state._currencyId];
                        account.SetState(state, caretaker);
                        item[state._currencyId] = account;
                        
                        item.TransactionProcessed?.Invoke();
                    }
                    else
                        unusedStates.Add(state);
                }

                return item;
            }
        }
    }
}
