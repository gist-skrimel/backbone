﻿namespace Skrimel.Gist.Economics
{
    public interface IEconomicAccount
    {
        uint this[Currency currency] { get; }
        
        void Process(Transaction transaction);
        bool Contains(Currency currency);
        bool OverlapsVolume(CurrencyVolume volume);
    }
}