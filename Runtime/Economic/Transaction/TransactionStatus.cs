﻿namespace Skrimel.Gist.Economics
{
    public enum TransactionStatus
    {
        Pending = 0,
        Succeed = 1,
        Failed = 2
    }
}