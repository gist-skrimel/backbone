﻿using System;

namespace Skrimel.Gist.Economics
{
    public struct Transaction : ITransaction
    {
        public CurrencyVolume Content { get; }
        public TransactionKind Kind { get; }
        
        public bool IsDeposit => Kind == TransactionKind.Deposit;
        public bool IsWithdraw => Kind == TransactionKind.Withdraw;
        
        public TransactionStatus Status { get; private set; }
        
        public bool IsPending => Status == TransactionStatus.Pending;
        public bool IsCompleted => !IsPending;
        public bool IsSucceed => Status == TransactionStatus.Succeed;
        public bool IsFailed => Status == TransactionStatus.Failed;
        
        public event Action<TransactionStatus> StatusChanged;
        public event Action Succeed;
        public event Action Failed;
        
        public Transaction(TransactionKind kind, CurrencyVolume content, Action<TransactionStatus> statusChanged,
            Action succeed, Action failed)
        {
            Status = TransactionStatus.Pending;
            Kind = kind;
            Content = content;
            
            StatusChanged = statusChanged;
            Succeed = succeed;
            Failed = failed;
        }
        
        public Transaction(TransactionKind kind, CurrencyVolume volume, Action onSucceed, Action onFailed)
            : this(kind, volume, (status) => {}, onSucceed, onFailed) {}
        
        public Transaction(TransactionKind kind, CurrencyVolume volume, Action<TransactionStatus> statusChanged)
            : this(kind, volume, statusChanged, () => {}, () => {}) {}
        
        public Transaction(TransactionKind kind, CurrencyVolume volume)
            : this(kind, volume, (status) => {}, () => {}, () => {}) {}
        public Transaction(TransactionKind kind, Currency currency, uint volume)
            : this(kind, new CurrencyVolume(currency, volume), (status) => {}, () => {}, () => {}) {}
        
        public void SetStatus(TransactionStatus status)
        {
            if (status == TransactionStatus.Pending)
                throw new OperationCanceledException("Transaction can not be marked as Pending");
            
            if (IsCompleted)
                throw new OperationCanceledException("Transaction is completed");
            
            Status = status;
            StatusChanged?.Invoke(Status);
            
            if (IsSucceed)
                Succeed?.Invoke();
            else
                Failed?.Invoke();
        }
        
        public void MarkAsFailed() =>
            SetStatus(TransactionStatus.Failed);
        
        public void MarkAsSucceed() =>
            SetStatus(TransactionStatus.Succeed);
    }
}