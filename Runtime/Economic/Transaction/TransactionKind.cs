﻿namespace Skrimel.Gist.Economics
{
    public enum TransactionKind
    {
        Withdraw = 0,
        Deposit = 1
    }
}