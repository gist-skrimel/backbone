﻿namespace Skrimel.Gist.Economics
{
    public interface ITransaction
    {
        TransactionKind Kind { get; }
        
        bool IsWithdraw { get; }
        bool IsDeposit { get; }
        
        TransactionStatus Status { get; }
        
        bool IsPending { get; }
        bool IsCompleted { get; }
        bool IsSucceed { get; }
        bool IsFailed { get; }
    }
}